Dado('que a sacola está vazia') do
    @home_page.pagina_principal
    expect(@home_page.verifica_sacola_vazia).to be true
end

Quando('realizo uma busca pelo {string}') do |codigo_produto|
    file = YAML.load_file(File.join(Dir.pwd, "features/support/fixtures/produtos.yaml"))
    @produtos = file[codigo_produto]

    @home_page.buscar_produto(@produtos)
end
  
Quando('adiciono o produto {string} na sacola') do |codigo_produto|    
    @home_page.resultado_busca_page.adicionar_produto(@produtos)
end
  
Então('o {string} é listado no carrinho') do |produto|
    expect(@home_page.sacola_page.verifica_produto_na_sacola(@produtos)).to be true
end

Quando('desisto do produto') do
    @home_page.sacola_page.desistir_do_produto_na_sacola
end
  
Então('vejo a mensagem da sacola {string}') do |mensagem_sacola|
    expect(@home_page.sacola_page.mensagem_sacola_vazia).to eql mensagem_sacola
end

Então('vejo a mensagem da busca {string} {string} {string}') do |msg1, produto, msg2|
    mensagem = msg1 + @produtos['descricao'] + msg2
    expect(@home_page.resultado_busca_page.mensagem_produto_nao_encontrado).to include mensagem
end