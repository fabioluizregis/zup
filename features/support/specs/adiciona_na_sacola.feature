#language: pt

Funcionalidade: Adicionar produto na sacola
    Como um cliente usuário do portal da loja
    Quero adicionar um produto na sacola
    Para validar que o produto do carrinho segue as mesmas especificações do produto adicionado
    
    @adiciona_produto
    Cenário: Adicionar produto na sacola
        Dado que a sacola está vazia
        Quando realizo uma busca pelo <codigo_produto>
        E adiciono o produto <codigo_produto> na sacola
        Então o <codigo_produto> é listado no carrinho

        Exemplos:
            | codigo_produto | 
            | "travesseiro"  | 
            | "pendrive"     | 
            | "cabo_hdmi"    | 

    @desiste_produto
    Cenário: Desistir do produto adicionado na sacola
        Dado que a sacola está vazia
        Quando realizo uma busca pelo <produto>
        E adiciono o produto <produto> na sacola
        E o <produto> é listado no carrinho
        Mas desisto do produto
        Então vejo a mensagem da sacola "Sua sacola está vazia"

        Exemplos:
            | produto       | 
            | "travesseiro" |
            | "cabo_hdmi"   | 

    @produto_inexistente
    Cenário: Produto inexistente no portal
        Dado que a sacola está vazia
        Quando realizo uma busca pelo <produto>
        Então vejo a mensagem da busca "Sua busca por " <produto> " não encontrou resultado algum :("
        
        Exemplos:
            | produto     |
            | "prod1_404" |
            | "prod2_404" |
        