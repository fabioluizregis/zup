class Sacola
    include Capybara::DSL

    def verifica_produto_na_sacola(produto)
        item = find('.BasketItemProduct-info-title')
        item.text.match? produto['descricao']
    end

    def desistir_do_produto_na_sacola
        find('.BasketItem-delete-label').click
    end

    def mensagem_sacola_vazia
        find('.EmptyBasket-title').text
    end
end