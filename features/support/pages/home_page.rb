class HomePage
    include Capybara:: DSL

    def pagina_principal
        visit "/"
    end

    def resultado_busca_page
        ResultadoBusca.new
    end

    def sacola_page
        Sacola.new
    end

    def verifica_sacola_vazia
        carrinho = find('.qtd-checkout')
        carrinho.text == "0"
    end

    def buscar_produto(produto)
        find('input[placeholder="procure por código, nome, marca..."]').set produto['descricao']
        find('#btnHeaderSearch').click
    end
end