class ResultadoBusca
    include Capybara::DSL

    def adicionar_produto(produto)
        find('.nm-product-name', text: produto['descricao']).click
        click_button "Adicionar à sacola"
    end
    
    def mensagem_produto_nao_encontrado
        find('#nm-not-found-page .nm-not-found-container').text
    end
end