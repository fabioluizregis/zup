require 'report_builder'

Before do
    @home_page = HomePage.new

    page.current_window.resize_to(1440,900)
end

After do |scenario|
        temp_shot = page.save_screenshot("log/temp_shot.png")
        screenshot = Base64.encode64(File.open(temp_shot, "rb").read)
        
        embed(screenshot, "image/png", "Screenshot")
end

at_exit do
    ReportBuilder.configure do |config|
    config.input_path = "log/report.json"
    config.report_path = "log/report"
    config.report_types = [:html]
    config.report_title = "Desafio ZUP - Automação Web - Magazine Luiza"
    config.compress_image = true
    config.additional_info = {"App" => "Web"}
    config.color = "indigo"
    end
    ReportBuilder.build_report
end