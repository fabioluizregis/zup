# Desafio ZUP #

Desafio de Automação ZUP.
Este desafio foi construído para um teste admissional para a ZUP.
Teste este utilizando RUBY, Capybara e Cucumber para a plataforma WEB.

## O que você precisa fazer para rodar esta suíte de testes? ###

* Clonar este projeto para sua máquina em uma pasta de sua escolha
``` bash
   git clone https://fabioluizregis@bitbucket.org/fabioluizregis/zup.git
```
* Instalar os drivers para os browsers suportados:
  
  Os drivers precisam estar em uma pasta que esteja no path do sistema.
  Como estou utilizando Windows para esta implementação, costumo jogar os executáveis na raiz da pasta Windows, que já está no PATH do sistema por padrão.
  * Chrome Browser: [CROMEDRIVER](https://sites.google.com/a/chromium.org/chromedriver/)
  * Firefox: [GECKODRIVER](https://developer.mozilla.org/en-US/docs/Web/WebDriver)


* Você precisará instalar o Ruby na sua máquina
  * Poderá ser baixado do site [Ruby Installer](https://rubyinstaller.org/)
  * Versão que estou utilizando para o projeto: ruby 2.6.6p146 (2020-03-31 revision 67876) [x64-mingw32]

* Bundler - Após clonar o projeto e instalar o Ruby, utilizar os comandos baixo, diretamente da pasta clonada do projeto em sua máquina, para rodar o projeto localmente:
```bash
  Gem install bundler
```
* Instalando as bibliotecas necessárias:
```bash
  bundle install
```
* Rodando o projeto:
```bash
  cucumber
```

## O que acontecerá após a execução do projeto?
Após a execução do projeto serão gerados os seguintes artefatos na pasta log/:
  * Relatório de evidências no formato html no caminho log/report.html
  * Relatório em formato JSON para uso em CI no caminho log/report.json

## Outras maneiras de executar o projeto:
  * cucumber -p <NOME_DO_PARÂMETRO> que pode ser encadeado.
  * Os parâmetros aceitos, estão abaixo. Se nenhum parâmetro for utilizado, por padrão, o cucumber roda o default.

```yaml

    default: -p test -p chrome -p json -p pretty
    ci: -p test -p headless -p json -p progress
    test: ENV_TYPE=test
    firefox: BROWSER=firefox
    chrome: BROWSER=chrome
    headless: BROWSER=headless
    html: --format html --out=log/report.html
    json: --format json --out=log/report.json
    pretty: --format pretty
    progress: --format progress

```

# Rodando no Jenkins

* Baixe, instale e configure o [Jenkins](https://www.jenkins.io/)

* Com o Jenkins instalado e rodando na tua máquina em http://localhost:8080/
* Crie um novo Job e escolha o tipo Pipeline
```
* Na aba pipeline escolha:
*   Definition : "Pipeline script from SCM"
*   SCM : "Git"
*   Repositories : 
*      Repository URL: https://fabioluizregis@bitbucket.org/fabioluizregis/zup.git
*      Credentials : "- none -"
*      Branch Specifier : "*/master"
*      Script Path : "Jenkinsfile"
*      Aplicar e depois Salvar
```

* Instale os plugins do cucumber, cucumber-reports e ruby
* execute o projeto